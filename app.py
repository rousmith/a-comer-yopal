from flask import Flask

app = Flask(__name__)


# Ruta "desayuno"
@app.route('/desayunos')
def desayunos():
    return "¡Bienvenido a desayunoS!"

# Ruta "almuerzo"
@app.route('/almuerzos')
def almuerzos():
    return "¡Bienvenido a almuerzos!"

# Ruta "cena"
@app.route('/cenas')
def cena():
    return "¡Bienvenido a cenas!"

if __name__ == '__main__':
    app.run()
